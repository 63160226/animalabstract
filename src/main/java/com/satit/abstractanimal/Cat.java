/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Cat extends LandAnimals{
     private String name;
    public Cat(String name) {
        super("Cog", 4);
        this.name =name;

    }
    @Override
    public void run() {
        System.out.println("Dog : " + name + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog : " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog : " + name + " walk");
    }

    @Override
    public void speed() {
        System.out.println("Dog : " + name + " speed");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : " + name + " sleep");
    }
}
