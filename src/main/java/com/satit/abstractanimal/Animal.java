/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public abstract class Animal {

    String name;
    int numberoflog;

    public Animal(String name, int numberofleg) {
        this.name = name;
        this.numberoflog = numberofleg;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberoflog() {
        return numberoflog;
    }

    public void setNumberoflog(int numberoflog) {
        this.numberoflog = numberoflog;
    }

    @Override
    public String toString() {
        return "Animal {" + "Type = " + name + " } Number of legs = " + numberoflog + " }";
    }

    public abstract void eat();

    public abstract void walk();

    public abstract void speed();

    public abstract void sleep();
}
