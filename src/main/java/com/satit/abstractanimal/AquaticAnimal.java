/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public abstract class AquaticAnimal extends Animal {

    public AquaticAnimal(String name, int numberofleg) {
        super(name, 0);
    }

    public abstract void swim();
}
