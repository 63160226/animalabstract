/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class crocodile extends lizard{
    private String name;

    public crocodile(String name) {
        super("crocodile",4);
        this.name=name;
    }
    
    @Override
    public void crawl() {
        System.out.println("crocodile: "+name+" crawl");
    }

    @Override
    public void eat() {
        System.out.println("crocodile: "+name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("crocodile: "+name+" walk");
    }

    @Override
    public void speed() {
        System.out.println("crocodile: "+name+" speed");
    }

    @Override
    public void sleep() {
        System.out.println("crocodile: "+name+" sleep");
    }
    
}
