/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Test {
    public static void main(String[] args) {
        Human h1 = new Human("Kim");
        h1.eat();
        h1.walk();
        h1.run();
        h1.sleep();
        System.out.println(h1);
        System.out.println("h1 is animal ? "+(h1 instanceof Animal));
        System.out.println("h1 is landanimal ? "+(h1 instanceof LandAnimals));
        Animal a1=h1;
        System.out.println("h1 is AquaticAnimal ? "+(a1 instanceof AquaticAnimal));
        System.out.println("h1 is poultryAnimal ? "+(a1 instanceof poultryAnimal));
        System.out.println("h1 is lizard ? "+(a1 instanceof lizard));
        Dog d1 = new Dog("Max");
        d1.eat();
        d1.run();
        d1.walk();
        d1.speed();
        d1.sleep();
        System.out.println(d1);
        System.out.println("d1 is animal ? "+(d1 instanceof Animal));
        System.out.println("d1 is landanimal ? "+(d1 instanceof LandAnimals));
        Animal a2=d1;
        System.out.println("d1 is AquaticAnimal ? "+(a2 instanceof AquaticAnimal));
        System.out.println("d1 is poultryAnimal ? "+(a2 instanceof poultryAnimal));
        System.out.println("d1 is lizard ? "+(a2 instanceof lizard));
        Cat c1 = new Cat("Nitty");
        c1.eat();
        c1.run();
        c1.walk();
        c1.speed();
        c1.sleep();
        System.out.println(c1);
        System.out.println("c1 is animal ? "+(c1 instanceof Animal));
        System.out.println("c1 is landanimal ? "+(c1 instanceof LandAnimals));
        Animal a3=c1;
        System.out.println("c1 is AquaticAnimal ? "+(a3 instanceof AquaticAnimal));
        System.out.println("c1 is poultryAnimal ? "+(a3 instanceof poultryAnimal));
        System.out.println("c1 is lizard ? "+(a3 instanceof lizard));
        Fish f1 = new Fish("Nemo");
        f1.eat();
        f1.swim();
        f1.walk();
        f1.speed();
        System.out.println(f1);
        System.out.println("f1 is animal ? "+(f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal ? "+(f1 instanceof AquaticAnimal));
        Animal a4=f1;
        System.out.println("f1 is landanimal ? "+(a4 instanceof LandAnimals));
        System.out.println("f1 is poultryAnimal ? "+(a4 instanceof poultryAnimal));
        System.out.println("f1 is lizard ? "+(a4 instanceof lizard));
        Bat b1 = new Bat("Oni");
        b1.eat();
        b1.fly();
        b1.walk();
        b1.sleep();
        b1.speed();
        System.out.println(b1);
        System.out.println("b1 is animal ? "+(b1 instanceof Animal));
        System.out.println("b1 is poultryAnimal ? "+(b1 instanceof poultryAnimal));
        Animal a5=b1;
        System.out.println("b1 is landanimal ? "+(a5 instanceof LandAnimals));
        System.out.println("b1 is AquaticAnimal ? "+(a5 instanceof AquaticAnimal));
        System.out.println("b1 is lizard ? "+(a5 instanceof lizard));
        Bird B1 = new Bird("Mega");
        B1.eat();
        B1.fly();
        B1.sleep();
        B1.walk();
        B1.speed();
        System.out.println(B1);
        System.out.println("B1 is animal ? "+(B1 instanceof Animal));
        System.out.println("B1 is poultryAnimal ? "+(B1 instanceof poultryAnimal));
        Animal a6=B1;
        System.out.println("B1 is landanimal ? "+(a6 instanceof LandAnimals));
        System.out.println("B1 is AquaticAnimal ? "+(a6 instanceof AquaticAnimal));
        System.out.println("B1 is lizard ? "+(a6 instanceof lizard));
        Snake s1 = new Snake("Mega");
        s1.eat();
        s1.crawl();
        s1.sleep();
        s1.speed();
        System.out.println(s1);
        System.out.println("B1 is animal ? "+(s1 instanceof Animal));
        System.out.println("B1 is lizard ? "+(s1 instanceof lizard));
        Animal a7=s1;
        System.out.println("B1 is landanimal ? "+(a7 instanceof LandAnimals));
        System.out.println("B1 is AquaticAnimal ? "+(a7 instanceof AquaticAnimal));
        System.out.println("B1 is poultryAnimal ? "+(a7 instanceof poultryAnimal));
        crocodile cr1 = new crocodile("Kami");
        cr1.eat();
        cr1.crawl();
        cr1.sleep();
        cr1.walk();
        cr1.speed();
        System.out.println(cr1);
        System.out.println("B1 is animal ? "+(cr1 instanceof Animal));
        System.out.println("B1 is lizard ? "+(cr1 instanceof lizard));
        Animal a8=cr1;
        System.out.println("B1 is landanimal ? "+(a8 instanceof LandAnimals));
        System.out.println("B1 is AquaticAnimal ? "+(a8 instanceof AquaticAnimal));
        System.out.println("B1 is poultryAnimal ? "+(a8 instanceof poultryAnimal));
    }
    
}
