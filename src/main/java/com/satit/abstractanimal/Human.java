/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Human extends LandAnimals {

    private String nickname;

    public Human(String nickname) {
        super("Human:", 2);
        this.nickname = nickname;

    }

    @Override
    public void run() {
        System.out.println("Human : " + nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Human : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Human : " + nickname + " walk");
    }

    @Override
    public void speed() {
        System.out.println("Human : " + nickname + " speed");
    }

    @Override
    public void sleep() {
        System.out.println("Human : " + nickname + " sleep");
    }
}
